# TMux Configuration

TMux configuration (fizzy-compliant)

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-tmux/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
